# Sukoe  Main
Repositorio principal de Sukoe.com

Este repositorio continene el API principal de [Sukoe](https://www.sukoe.com)

[Soluciones Tecnológicas para el Sector Farmacéutico:](https://www.sukoe.com)  

* Software a Medida
* Inteligencia Artificial
* Plataforma Tecnológica

Puedes ver un ejemplo en [Sukoe Software](https://www.sukoe.com/software.html)

-------

Main Repository for Sukoe.com

This repository contain the main API for [Sukoe](https://www.sukoe.com/en)  

[Technology Solutions for the Pharmaceutical Sector:](https://www.sukoe.com/en)  

* Tailor-Made Software
* Artificial Intelligence
* Technology Platform

You can see an example in [Sukoe Software](https://www.sukoe.com/en/software.html)


